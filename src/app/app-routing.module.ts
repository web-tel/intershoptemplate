import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {IndexPageComponent} from './index-page/index-page.component';
import {AboutComponent} from './about/about.component';
import {ProductsComponent} from './products/products.component';
import {RegAndAuthComponent} from './reg-and-auth/reg-and-auth.component';
import {AuthGuard} from './auth.guard';
import {UseCookieGuard} from './use-cookie.guard';
import {TermsOfUseComponent} from './terms-of-use/terms-of-use.component';



const routes: Routes = [
  { path: '', component: IndexPageComponent, pathMatch: 'full', canActivate: [UseCookieGuard]},
  { path: 'products.html', component: ProductsComponent},
  { path: 'about.html', component: AboutComponent},
  {path: 'reg-and-auth', component: RegAndAuthComponent},
  { path: 'terms-of-use.html', component: TermsOfUseComponent, pathMatch: 'full'},
  {path: 'personal-area', loadChildren: () => import('./personal-area/personal-area.module').then(m => m.PersonalAreaModule), canActivate: [AuthGuard]},
  { path: '**', component: RegAndAuthComponent }
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
