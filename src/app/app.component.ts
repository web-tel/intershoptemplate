import { Component } from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  showCookieMessage = true;

  constructor(private meta: Meta) {
    meta.addTags([
      {name: 'keywords', content: 'Магазин компьютерной техники'},
      {name: 'description', content: 'Магазин компьютерной техники'},
    ]);

    if (localStorage.getItem('useCookie')) {
      this.showCookieMessage = false;
    }
  }

  useCookie() {
    this.showCookieMessage = false;
    localStorage.setItem('useCookie', 'true');
  }
}
