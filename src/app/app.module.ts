import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { IndexPageComponent } from './index-page/index-page.component';
import { PagesComponent } from './pages/pages.component';
import { AboutComponent } from './about/about.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ProductsComponent } from './products/products.component';
import {AppRoutingModule} from './app-routing.module';
import { IndexPageCarouselComponent } from './items/index-page-carousel/index-page-carousel.component';
import { ProductItemComponent } from './items/product-item/product-item.component';
import { CarouselItemComponent } from './items/carousel-item/carousel-item.component';
import {PersonalAreaModule} from './personal-area/personal-area.module';
import { RegAndAuthComponent } from './reg-and-auth/reg-and-auth.component';
import {ToastrModule} from "ngx-toastr";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { TermsOfUseComponent } from './terms-of-use/terms-of-use.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexPageComponent,
    PagesComponent,
    AboutComponent,
    HeaderComponent,
    FooterComponent,
    ProductsComponent,
    IndexPageCarouselComponent,
    ProductItemComponent,
    CarouselItemComponent,
    RegAndAuthComponent,
    TermsOfUseComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      closeButton: true,
    }),
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
