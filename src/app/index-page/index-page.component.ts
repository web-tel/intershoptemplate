import { Component, OnInit } from '@angular/core';
import {CarouselItem, ProductItem} from '../../entities/data';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-index-page',
  templateUrl: './index-page.component.html',
  styleUrls: ['./index-page.component.css']
})
export class IndexPageComponent implements OnInit {
  products: ProductItem[] = [];
  carouselItems: CarouselItem[] = [];

  constructor(private title: Title) {
    title.setTitle('Магазин компьютерной техники');
  }

  ngOnInit(): void {
    this.getCarouselItems();
    this.getProducts();
  }


  getCarouselItems() {
    this.carouselItems = [
      {id: '1', title: 'Акция 1', text: 'Описание акции 1', imgUrl: 'assets/testing_static/akciya1.jpg', itemUrl: ''},
      {id: '1', title: 'Акция 2', text: 'Описание акции 2', imgUrl: 'assets/testing_static/akciya2.jpg', itemUrl: ''},
    ];
  }

  getProducts() {
    this.products = [
      {id: '1', title: 'Модуль пам\'яті DDR4 2 х 8GB 3000MHz G.Skill Original Trident Z Royal CL 16 (F4-3000C16D-16', imgUrl: 'assets/testing_static/product1.jpg', price: '100'},
      {id: '2', title: 'Модуль пам\'яті DDR4 2 х 8GB 3000MHz G.Skill SniperX Urban Camo 1.35V CL16 (F4-3000C16D-16G', imgUrl: 'assets/testing_static/product2.jpg', price: '100'},
      {id: '3', title: 'Модуль пам\'яті DDR4 2 х 8GB 3200MHz G.Skill Aegis C16-18-18-38 (F4-3200C16D-16GIS)', imgUrl: 'assets/testing_static/product3.jpg', price: '100'},
      {id: '4', title: 'Модуль пам\'яті DDR4 2 х 8GB 3200MHz G.Skill FlareX Black 1.35V C16-18-18-38 (F4-3200C16D-1', imgUrl: 'assets/testing_static/product4.jpg', price: '100'},
      {id: '5', title: 'Модуль пам\'яті DDR4 2 х 8GB 3200MHz G.Skill Ripjaws V (F4-3200C16D-16GVKB)', imgUrl: 'assets/testing_static/product5.jpg', price: '100'},
      {id: '6', title: 'Модуль пам\'яті DDR4 2 х 8GB 3200MHz Kingston HyperX Fury Black C18-21-21 (HX432C16FB3K2/16', imgUrl: 'assets/testing_static/product6.jpg', price: '100'}
    ]
  }

}
