import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexPageCarouselComponent } from './index-page-carousel.component';

describe('IndexPageCarouselComponent', () => {
  let component: IndexPageCarouselComponent;
  let fixture: ComponentFixture<IndexPageCarouselComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexPageCarouselComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexPageCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
