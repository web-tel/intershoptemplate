import {Component, Input, OnInit} from '@angular/core';
import {CarouselItem} from "../../../entities/data";

@Component({
  selector: 'app-index-page-carousel',
  templateUrl: './index-page-carousel.component.html',
  styleUrls: ['./index-page-carousel.component.css']
})
export class IndexPageCarouselComponent implements OnInit {

  @Input() carouselItems: CarouselItem[];

  constructor() { }

  ngOnInit(): void {
  }

}
