import {Component, Input, OnInit} from '@angular/core';
import {ProductItem} from '../../../entities/data';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {Title} from '@angular/platform-browser';
import {HelperService} from '../../../services/helper.service';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit {

  @Input() productItem: ProductItem;

  constructor(private authService: AuthService, private toastr: ToastrService, public helperService: HelperService) { }

  ngOnInit(): void {
  }

  addToBasket(id: string) {
    this.authService.getAuthStatus().then(isAuth =>{
      if (isAuth) {
        this.toastr.success('Товар добавлен в корзину', 'Уведомление');
      }
      else {
        this.toastr.error('Необходима авторизация', 'Уведомление');
        //this.router.navigate(['/reg-and-auth']);
      }
    });
  }
}
