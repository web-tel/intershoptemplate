import { Component, OnInit } from '@angular/core';
import {ProductInBasket} from '../../../entities/data';
import {HelperService} from '../../../services/helper.service';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {

  products: ProductInBasket[] = [];

  constructor(public helperService: HelperService) { }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.products = [
      {id: '1', title: 'Модуль пам\'яті DDR4 2 х 8GB 3000MHz G.Skill Original Trident Z Royal CL 16 (F4-3000C16D-16',
        imgUrl: 'assets/testing_static/product1.jpg', price: '50', productId: '1', quantity: 3},
      {id: '2', title: 'Модуль пам\'яті DDR4 2 х 8GB 3000MHz G.Skill SniperX Urban Camo 1.35V CL16 (F4-3000C16D-16G',
        imgUrl: 'assets/testing_static/product2.jpg', price: '100', productId: '2', quantity: 5},
      {id: '3', title: 'Модуль пам\'яті DDR4 2 х 8GB 3200MHz G.Skill Aegis C16-18-18-38 (F4-3200C16D-16GIS)',
        imgUrl: 'assets/testing_static/product3.jpg', price: '150', productId: '3', quantity: 7},
    ]
  }


  cartTotal() {
    let totalPrice = 0;

    for (var item of this.products) {
        totalPrice = totalPrice + (parseFloat(item.price) * item.quantity);
    }

    return totalPrice;
  }

  delItem() {

  }

  createOrder() {

  }
}
