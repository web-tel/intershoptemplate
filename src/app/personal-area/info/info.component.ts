import { Component, OnInit } from '@angular/core';
import {UserInfo} from "../../../entities/data";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {

  userInfo: UserInfo;

  UserInfoForm: FormGroup = new FormGroup({
      email: new FormControl('', [Validators.email]),
      firstName: new FormControl('', []),
      lastName: new FormControl('', []),
      phone: new FormControl('', []),
      telegramId: new FormControl('', []),
  });

  constructor() { }

  ngOnInit(): void {
    this.getUserInfo();
  }

  getUserInfo() {
    this.userInfo = {id: '1', email: 'podwar2008@gmail.com', firstName: 'Ivan', lastName: 'Ivanox',
      phone: '+380979702006', telegramId: '1', userId: '1'};
  }

  updateUserInfo() {

  }
}
