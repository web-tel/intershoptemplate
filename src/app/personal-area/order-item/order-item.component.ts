import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-order-item',
  templateUrl: './order-item.component.html',
  styleUrls: ['./order-item.component.css']
})
export class OrderItemComponent implements OnInit {

  @Input() order;
  showDetails = false;
  constructor() { }

  ngOnInit(): void {
  }

  getBsTarget() {
    return `#order${this.order.id}`
  }

  changeShowDetails() {
    this.showDetails = !this.showDetails;
  }
}
