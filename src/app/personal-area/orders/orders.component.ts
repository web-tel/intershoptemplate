import { Component, OnInit } from '@angular/core';
import {Order} from "../../../entities/data";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  orders: Order[];

  constructor() { }

  ngOnInit(): void {
    this.orders = [
      {id: '1', cost: '500', quantity: 2, status: 'new',
        product: {id: '1', title: 'Модуль пам\'яті DDR4 2 х 8GB 3000MHz G.Skill Original Trident Z Royal CL 16 (F4-3000C16D-16', imgUrl: 'assets/testing_static/product1.jpg', price: '100'}
      },
      {id: '2', cost: '1500', quantity: 1, status: 'canceled',
        product: {id: '2', title: 'Модуль пам\'яті DDR4 2 х 8GB 3000MHz G.Skill SniperX Urban Camo 1.35V CL16 (F4-3000C16D-16G', imgUrl: 'assets/testing_static/product2.jpg', price: '100'}
        },
      {id: '3', cost: '5500', quantity: 3, status: 'completed',
        product: {id: '3', title: 'Модуль пам\'яті DDR4 2 х 8GB 3200MHz G.Skill Aegis C16-18-18-38 (F4-3200C16D-16GIS)', imgUrl: 'assets/testing_static/product3.jpg', price: '100'}
      },
    ];
  }




}
