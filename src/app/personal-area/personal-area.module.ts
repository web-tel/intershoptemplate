import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneralComponent } from './general/general.component';
import { BasketComponent } from './basket/basket.component';
import {RouterModule, Routes} from '@angular/router';
import { InfoComponent } from './info/info.component';
import { OrdersComponent } from './orders/orders.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { OrderItemComponent } from './order-item/order-item.component';

const paRoutes: Routes = [
    { path: '', component: GeneralComponent, children: [
      { path: '', component: InfoComponent},
      { path: 'basket.html', component: BasketComponent},
        { path: 'orders.html', component: OrdersComponent},
      ]},
    { path: '**', component: GeneralComponent }
];

@NgModule({
  declarations: [GeneralComponent, BasketComponent, InfoComponent, OrdersComponent, OrderItemComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(paRoutes),
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [RouterModule],
})
export class PersonalAreaModule { }
