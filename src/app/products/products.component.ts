import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";
import {Category, ProductItem} from "../../entities/data";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  categoryItems: Category[] = [];
  products: ProductItem[] = [];
  noProducts = false;

  constructor(private title: Title) {
    title.setTitle('Все товары');
  }

  ngOnInit(): void {
    this.getCategoryItems();
    this.getProducts();
  }

  getProducts() {
    this.products = [
      {id: '1', title: 'Модуль пам\'яті DDR4 2 х 8GB 3000MHz G.Skill Original Trident Z Royal CL 16 (F4-3000C16D-16', imgUrl: 'assets/testing_static/product1.jpg', price: '100'},
      {id: '2', title: 'Модуль пам\'яті DDR4 2 х 8GB 3000MHz G.Skill SniperX Urban Camo 1.35V CL16 (F4-3000C16D-16G', imgUrl: 'assets/testing_static/product2.jpg', price: '100'},
      {id: '3', title: 'Модуль пам\'яті DDR4 2 х 8GB 3200MHz G.Skill Aegis C16-18-18-38 (F4-3200C16D-16GIS)', imgUrl: 'assets/testing_static/product3.jpg', price: '100'},
      {id: '4', title: 'Модуль пам\'яті DDR4 2 х 8GB 3200MHz G.Skill FlareX Black 1.35V C16-18-18-38 (F4-3200C16D-1', imgUrl: 'assets/testing_static/product4.jpg', price: '100'},
      {id: '5', title: 'Модуль пам\'яті DDR4 2 х 8GB 3200MHz G.Skill Ripjaws V (F4-3200C16D-16GVKB)', imgUrl: 'assets/testing_static/product5.jpg', price: '100'},
      {id: '6', title: 'Модуль пам\'яті DDR4 2 х 8GB 3200MHz Kingston HyperX Fury Black C18-21-21 (HX432C16FB3K2/16', imgUrl: 'assets/testing_static/product6.jpg', price: '100'}
    ]
  }

  getCategoryItems(categoryId: string = null) {
    if (categoryId) {
      this.categoryItems = [
        {id: '4', title: 'Процессоры', desc: '', masterId: null},
        {id: '5', title: 'Видео карты', desc: '', masterId: null},
        {id: '6', title: 'Память', desc: '', masterId: null},
      ];
    }
    else {
      this.categoryItems = [
        {id: '1', title: 'Компьютеры', desc: '', masterId: null},
        {id: '2', title: 'Ноутбуки', desc: '', masterId: null},
        {id: '3', title: 'Телевизоры', desc: '', masterId: null},
      ];
    }
  }

  selectCategory(id: string) {
    this.getCategoryItems(id);
  }

  showMoreProducts() {
    this.getProducts();
  }
}
