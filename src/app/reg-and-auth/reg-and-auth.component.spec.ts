import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegAndAuthComponent } from './reg-and-auth.component';

describe('RegAndAuthComponent', () => {
  let component: RegAndAuthComponent;
  let fixture: ComponentFixture<RegAndAuthComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegAndAuthComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegAndAuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
