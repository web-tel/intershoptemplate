import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {FormControl, FormGroup, Validators, FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import { ConfirmedValidator} from '../custom.validators';

@Component({
  selector: 'app-reg-and-auth',
  templateUrl: './reg-and-auth.component.html',
  styleUrls: ['./reg-and-auth.component.css']
})
export class RegAndAuthComponent implements OnInit {

   RegForm: FormGroup = new FormGroup({});


  constructor(private authService: AuthService, private toastr: ToastrService, private fb: FormBuilder) {
    this.RegForm = fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      password2: ['', [Validators.required, Validators.minLength(8)]],
      accept_terms: ['', [Validators.required, Validators.requiredTrue]]
    }, {
      validator: ConfirmedValidator('password', 'password2')
    });
  }



  LoginForm: FormGroup = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
  });

  ngOnInit(): void {

  }

  loginAfterReg(email: string, password: string) {
    this.authService.login();
    this.toastr.success('Вы успешно ваторизировались', 'Уведомление');
  }

  submitRegistration() {
        if (this.RegForm.status === 'VALID') {
          if (this.RegForm.controls.password.value !== this.RegForm.controls.password2.value) {
            console.log(this.RegForm.controls.accept_terms);
            this.toastr.warning('Пароли не совпадают', 'Уведомление');
          }
          else {
            this.toastr.success('Вы зарегестрированы', 'Уведомление');
          }
      }
    }


    submitLogin() {
      if (this.LoginForm.status === 'VALID') {
        this.loginAfterReg(this.LoginForm.controls.email.value, this.LoginForm.controls.password.value);
      }
    }


}
