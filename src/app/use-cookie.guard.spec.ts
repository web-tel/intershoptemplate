import { TestBed } from '@angular/core/testing';

import { UseCookieGuard } from './use-cookie.guard';

describe('UseCookieGuard', () => {
  let guard: UseCookieGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(UseCookieGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
