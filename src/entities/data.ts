export interface CarouselItem {
  id: string;
  imgUrl: string;
  title: string;
  text: string;
  itemUrl: string;
}

export interface ProductItem {
  id: string;
  title: string;
  price: string;
  imgUrl: string;
}

export interface Category {
  id: string;
  title: string;
  desc: string;
  masterId: string;
}

export interface ProductInBasket {
  id: string;
  productId: string;
  title: string;
  price: string;
  imgUrl: string;
  quantity: number;
}

export interface UserInfo {
  id: string;
  userId: string;
  email: string;
  phone: string;
  firstName: string;
  lastName: string;
  telegramId: string;
}

export interface Order {
  id: string;
  product: ProductItem;
  quantity: number;
  cost: string;
  status: string;
}
